use std::{ffi::OsString, iter::once, path::PathBuf};

#[derive(Debug, Clone)]
pub struct Config {
    pub name: OsString,
    pub pass: PathBuf,
    pub dir: PathBuf,
    pub find: PathBuf,
    pub picker: PathBuf,
    pub picker_args: Vec<OsString>,
    pub typist: PathBuf,
    pub typist_args: Vec<OsString>,
}
impl Default for Config {
    fn default() -> Self {
        Self {
            name: "pass-fu".into(),
            dir: "~/.password-store".into(),
            pass: "/usr/bin/pass".into(),
            find: "find".into(),
            picker: "dmenu".into(),
            picker_args: vec![],
            typist: "xdotool".into(),
            typist_args: ["type", "--clearmodifiers", "--file", "-"]
                .into_iter()
                .map(|a| a.into())
                .collect(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Opt {
    pub config: Config,
    pub command: Command,
}
impl AsRef<Opt> for Opt {
    fn as_ref(&self) -> &Opt {
        self
    }
}
impl Opt {
    pub fn parse(
        args: impl IntoIterator<Item = OsString>,
        env: impl IntoIterator<Item = (OsString, OsString)>,
    ) -> Result<Opt, impl std::fmt::Display> {
        let env = env.into_iter();
        let mut args = args.into_iter();
        let mut config = Config::default();
        config.name = args.next().unwrap_or(config.name);

        let command = Command::parse(args);
        for (key, val) in env {
            match key.to_str() {
                Some("PASSWORD_STORE_DIR") => config.dir = val.into(),
                _ => {}
            }
        }
        Result::<Opt, &str>::Ok(Self { config, command })
    }
}

#[derive(Debug, Clone)]
pub enum Command {
    Show(ShowOpt),
    Other(PassOpt),
}
impl Command {
    fn parse(args: impl IntoIterator<Item = OsString>) -> Command {
        let mut args = args.into_iter();
        // unfortunately, pass CLI interface is ambiguous with commands vs paths.
        // an easy fix would be to demand paths to start with "/" or command to always be
        // explicitly specified.
        match args.next() {
            None => Self::parse_show(args),
            Some(arg) => match arg.to_str() {
                None => Self::parse_other(arg, args),
                Some(cmd) => match cmd {
                    "--help" | "--version" | "init" | "find" | "grep" | "insert" | "edit"
                    | "generate" | "rm" | "mv" | "cp" | "git" | "help" | "version" | "ls"                     
                    // TODO: avoid other extensions?
                    | "otp" => {
                        Self::parse_other(arg, args)
                    }
                    "show" => Self::parse_show(args),
                    _positional => Self::parse_show(once(arg).chain(args)),
                },
            },
        }
    }
    fn parse_other(command: OsString, args: impl IntoIterator<Item = OsString>) -> Command {
        Self::Other(PassOpt {
            command,
            rest: args.into_iter().collect(),
        })
    }
    fn parse_show(args: impl IntoIterator<Item = OsString>) -> Command {
        let mut args = args.into_iter();
        let mut line = String::new();
        let mut mode = ShowMode::default();
        let mut path = None;
        let mut rest = vec![];
        let mut options_done = false;
        while let Some(arg) = args.next() {
            match arg.to_str().and_then(|a| {
                // extract options with this style:
                // --option, -o, --option=value, -ovalue
                // short options are not banged together in pass
                a.split_once("=")
                    .or_else(|| Some((a, "")))
                    .filter(|(o, _)| o.starts_with("--"))
                    .or_else(|| a.len().checked_sub(2).map(|_| a.split_at(2)))
                    .filter(|(o, _)| o.starts_with("-"))
                    .filter(|_| !options_done)
            }) {
                Some(("-c", v)) | Some(("--clip", v)) => {
                    mode = ShowMode::Clip;
                    line = v.to_owned();
                }
                Some(("-q", v)) | Some(("--qrcode", v)) => {
                    mode = ShowMode::QrCode;
                    line = v.to_owned();
                }
                Some(("-t", v)) | Some(("--type", v)) => {
                    mode = ShowMode::Type;
                    line = v.to_owned();
                }
                Some(("-o", v)) | Some(("--output", v)) => {
                    mode = ShowMode::Output;
                    line = v.to_owned();
                }
                Some(("--", "")) => {
                    options_done = true;
                    rest.push(arg);
                }
                Some(_option) => rest.push(arg),
                None => {
                    // no option detected, this is the path
                    // pass just takes the first and ignores the rest if multiple are specified
                    path = path.or(Some(PathBuf::from(arg)));
                }
            }
        }
        Command::Show(ShowOpt {
            path,
            line,
            mode,
            rest,
        })
    }
}

#[derive(Debug, Default, Clone)]
pub struct ShowOpt {
    pub path: Option<PathBuf>,
    pub line: String,
    pub mode: ShowMode,
    pub rest: Vec<OsString>,
}
#[derive(Debug, Clone)]
pub enum ShowMode {
    Output,
    Clip,
    QrCode,
    Type,
}
impl Default for ShowMode {
    fn default() -> Self {
        ShowMode::Output
    }
}
#[derive(Debug, Default, Clone)]
pub struct PassOpt {
    pub command: OsString,
    pub rest: Vec<OsString>,
}
