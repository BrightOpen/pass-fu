use crate::{
    err::{assert_child_io, assert_child_status, Errors, LogicError, PassFuResult},
    opt::{Command, Config, Opt, PassOpt, ShowMode, ShowOpt},
};
use std::{
    ffi::OsString,
    io::{self, BufRead, BufReader, Write},
    iter::once,
    path::{Component, PathBuf},
    process::Stdio,
};

pub fn main() {
    if let Err(err) = run(std::env::args_os(), std::env::vars_os()) {
        eprintln!("Error {err}");
        std::process::exit(err.code())
    }
}

pub fn run(
    args: impl IntoIterator<Item = OsString>,
    env: impl IntoIterator<Item = (OsString, OsString)>,
) -> PassFuResult<()> {
    let Opt {
        config, command, ..
    } = Opt::parse(args, env)
        .map_err(LogicError::new)
        .ctx("parsing arguments")?;

    match command {
        Command::Show(opt) => search(config, opt),
        Command::Other(opt) => pass_through(config, opt),
    }
}
fn pass_through(cfg: Config, opt: PassOpt) -> PassFuResult<()> {
    let status = std::process::Command::new(&cfg.pass)
        .args(once(opt.command).chain(opt.rest))
        .status();

    assert_child_status(cfg.pass.to_str(), status)
}

fn search(cfg: Config, mut opt: ShowOpt) -> PassFuResult<()> {
    opt.path = {
        let paths = find(&cfg, &opt).ctx("finding paths")?;
        pick(&cfg, paths).ctx("picking a path")?.map(|p| p.into())
    };

    const OTP_CODE: &str = "OTP code";
    let (mut content, line) = if opt.line.is_empty() {
        // we do not know exactly which part of the file do we need.
        let content = fetch(&cfg, &opt).ctx("fetching content")?;

        let choice = {
            // ask if a single line or whole file
            let mut choices = vec![];
            for (i, l) in content.iter().enumerate() {
                let mut l = l.as_str();
                if l.trim().starts_with("otpauth://") {
                    l = "OTP URI";
                    choices.push(format!("{i}: {OTP_CODE}"));
                } else if i == 0 {
                    l = "the main password";
                } else {
                    l = "*******";
                }
                choices.push(format!("{i}: {l}"));
            }

            if let [single] = &mut choices[..] {
                std::mem::take(single)
            } else {
                choices.insert(0, "All".to_owned());
                pick(&cfg, choices)
                    .ctx("picking a line")?
                    .unwrap_or_default()
            }
        };

        let (line, kind) = choice
            .split_once(":")
            .map(|(line, kind)| (line.parse::<usize>().ok(), kind))
            .unwrap_or_default();

        if kind.trim() == OTP_CODE {
            let code = match line {
                Some(l) => otp(&content[l])?,
                None => otp(&content.join("\n"))?,
            };
            (vec![code], "0".to_owned())
        } else {
            (content, line.map(|l| l.to_string()).unwrap_or_default())
        }
    } else {
        (vec![], opt.line)
    };
    opt.line = line;

    match &opt.mode {
        ShowMode::Clip | ShowMode::QrCode => {
            // we have a copy or qr request on command line
            // this will be taken care of by pass now
            // might avoid loading the content
            return send(&cfg, &opt);
        }
        _ => {
            // we will handle it
        }
    }

    if content.is_empty() {
        content = fetch(&cfg, &opt).ctx("loading content2")?;
    }
    eprintln!("line {}", opt.line);
    let secret = if opt.line.is_empty() {
        content.join("\n")
    } else {
        let line = opt
            .line
            .parse::<usize>()
            .map_err(LogicError::new)
            .ctx(format!("parsing line number for {:?} option", opt.mode))?;
        content.get(line).map(|l| l.to_owned()).unwrap_or_default()
    };

    match opt.mode {
        ShowMode::Output => print!("{}", secret),
        ShowMode::Clip => unreachable!("this is handled by pass"),
        ShowMode::QrCode => unreachable!("this is handled by pass"),
        ShowMode::Type => typeit(&cfg, &secret)?,
    }

    Ok(())
}
/// Find all relevant entries
fn find(cfg: &Config, opt: &ShowOpt) -> PassFuResult<Vec<String>> {
    let mut base = cfg.dir.clone();
    if let Some(ref p) = opt.path {
        // we consider it a base folder or an explicit file like pass
        base.extend(p.components().filter(|c| !matches!(c, Component::RootDir)));
        if !base.exists() {
            // not an existing dir... is it a file?
            let mut basefile = base.as_os_str().to_owned();
            basefile.push(".gpg");
            let basefile: PathBuf = basefile.into();
            if basefile.exists() && (basefile.is_file() || basefile.is_symlink()) {
                // it's a file, no finding needed!
                return Ok(vec![p.to_str().unwrap_or_default().to_owned()]);
            } else {
                // it's not a dir nor file, this is probably going to fail.
                eprintln!("base path does not exist {basefile:?}");
            }
        }
    }

    base =
        assert_child_io(&cfg.find, base.canonicalize()).ctx(format!("searching dir {base:?}"))?;

    // find "$PASSWORD_STORE_DIR" -iname '*.gpg'
    let child = std::process::Command::new(&cfg.find)
        .arg(base)
        .args(["-iname", "*.gpg"])
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .spawn();
    let mut child = assert_child_io(&cfg.find, child)?;
    let mut paths = vec![];
    for line in BufReader::new(child.stdout.as_mut().expect("find output as instructed")).lines() {
        let path = assert_child_io(
            &cfg.find,
            PathBuf::from(assert_child_io(&cfg.find, line)?)
                .with_extension("")
                .strip_prefix(&cfg.dir)
                .map(|path| path.to_str().unwrap_or_default().to_owned())
                .map_err(|e| io::Error::new(io::ErrorKind::InvalidData, e)),
        )?;
        paths.push(path);
    }

    assert_child_status(&cfg.find, child.wait())?;
    Ok(paths)
}
/// Let the user chose which entry to use
fn pick(cfg: &Config, items: Vec<String>) -> PassFuResult<Option<String>> {
    if items.len() == 0 {
        // no entries, empty
        return Ok(None);
    }
    if items.len() == 1 {
        // single entry, found
        return Ok(items.into_iter().next());
    }

    // multiple entries, invoke picker
    let child = std::process::Command::new(&cfg.picker)
        .args(&cfg.picker_args)
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .spawn();
    let mut child = assert_child_io(&cfg.picker, child)?;

    let picker = cfg.picker.clone();
    let mut feed = child.stdin.take().expect("picker input as instructed");
    std::thread::spawn(move || {
        for path in items {
            assert_child_io(&picker, feed.write_all(path.as_bytes())).unwrap();
            assert_child_io(&picker, feed.write_all(b"\n")).unwrap();
        }
        assert_child_io(&picker, feed.flush()).unwrap();
    });

    let mut paths = vec![];
    for path in BufReader::new(child.stdout.as_mut().expect("picker output as instructed")).lines()
    {
        paths.push(assert_child_io(&cfg.picker, path)?);
    }

    assert_child_status(&cfg.picker, child.wait())?;

    Ok(match &mut paths[..] {
        [] => Err(LogicError::new("No entries picked")),
        [_, _, ..] => Err(LogicError::new("Too many entries picked")),
        [single] => Ok(Some(std::mem::take(single))),
    }?)
}
/// Fetch the pass secret
fn fetch(cfg: &Config, opt: &ShowOpt) -> PassFuResult<Vec<String>> {
    let child = std::process::Command::new(&cfg.pass)
        .args(&opt.path)
        .stdout(Stdio::piped())
        .stderr(Stdio::inherit())
        .stdin(Stdio::piped())
        .spawn();
    let mut child = assert_child_io(&cfg.pass, child)?;

    let mut lines = vec![];

    for line in BufReader::new(child.stdout.as_mut().expect("pass output as instructed")).lines() {
        lines.push(assert_child_io(&cfg.pass, line)?);
    }

    assert_child_status(&cfg.pass, child.wait())?;
    Ok(lines)
}

/// Generate OTP token
fn otp(otpauth_url: &str) -> PassFuResult<String> {
    let totp = totp_rs::TOTP::from_url(otpauth_url)?;
    Ok(totp.generate_current()?)
}
/// Send final instructions to pass
fn send(cfg: &Config, opt: &ShowOpt) -> PassFuResult<()> {
    let option_fmt = |o: &str, v: &String| {
        if v.is_empty() {
            o.to_owned()
        } else {
            format!("{o}={v}")
        }
    };
    let mode_arg = match opt.mode {
        ShowMode::Output if opt.line.is_empty() => {
            /*noop*/
            None
        }
        ShowMode::Clip => Some(option_fmt("--clip", &opt.line)),
        ShowMode::QrCode => Some(option_fmt("--qrcode", &opt.line)),
        ref mode => unimplemented!("The original pass does not implement {mode:?}"),
    };

    let status = std::process::Command::new(&cfg.pass)
        .args(mode_arg)
        .args(&opt.path)
        .status();

    assert_child_status(&cfg.pass, status)
}

/// Send final instructions to pass
fn typeit(cfg: &Config, secret: &str) -> PassFuResult<()> {
    let child = std::process::Command::new(&cfg.typist)
        .args(&cfg.typist_args)
        .stdout(Stdio::inherit())
        .stderr(Stdio::inherit())
        .stdin(Stdio::piped())
        .spawn();
    let mut child = assert_child_io(&cfg.picker, child)?;
    let feed = child.stdin.as_mut().expect("typist input as instructed");
    assert_child_io(&cfg.typist, feed.write_all(secret.as_bytes())).unwrap();
    assert_child_status(&cfg.pass, child.wait())
}
