use std::{backtrace::Backtrace, fmt, io};

pub type PassFuResult<T> = Result<T, PassFuError>;

pub trait Errors {
    type Contextual;
    fn ctx(self, context: impl fmt::Display) -> Self::Contextual;
}
impl<T, E> Errors for Result<T, E>
where
    E: Into<PassFuError>,
{
    type Contextual = Result<T, PassFuError>;
    fn ctx(self, context: impl fmt::Display) -> Self::Contextual {
        self.map_err(|e| e.into().ctx(context))
    }
}

#[derive(Debug)]
pub struct PassFuError {
    msg: String,
    trouble: Trouble,
    backtrace: Backtrace,
}
impl std::error::Error for PassFuError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        Some(&self.trouble)
    }
}
impl fmt::Display for PassFuError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let Self { msg, trouble, .. } = self;
        write!(f, "{msg} - {trouble}")
    }
}

impl PassFuError {
    #[inline]
    fn new(trouble: impl Into<Trouble>) -> Self {
        Self {
            msg: String::default(),
            trouble: trouble.into(),
            backtrace: Backtrace::capture(),
        }
    }
    fn ctx(mut self, context: impl fmt::Display) -> Self {
        let context = context.to_string();
        if !self.msg.is_empty() {
            self.msg.insert_str(0, " - ");
        }
        self.msg.insert_str(0, &context.to_string());
        self
    }
    pub fn code(&self) -> i32 {
        match self.trouble {
            Trouble::Logic(_) => 255,
            Trouble::ChildIo(_) => 253,
            Trouble::Totp(_) => 252,
            Trouble::TotpTime(_) => 251,
            Trouble::Child(ChildExitStatus(code)) => code,
        }
    }
}
impl<T> From<T> for PassFuError
where
    Trouble: From<T>,
{
    fn from(err: T) -> Self {
        Self::new(Trouble::from(err))
    }
}

#[derive(Debug, thiserror::Error)]
enum Trouble {
    #[error(transparent)]
    Logic(#[from] LogicError),
    #[error(transparent)]
    Child(#[from] ChildExitStatus),
    #[error(transparent)]
    ChildIo(#[from] ChildIoError),
    #[error("TOTP code generation failed: {0:?}")]
    Totp(#[from] totp_rs::TotpUrlError),
    #[error("TOTP code timing failed: {0:?}")]
    TotpTime(#[from] std::time::SystemTimeError),
}
#[derive(Debug, thiserror::Error)]
#[error("child process returned {0}")]
pub(crate) struct ChildExitStatus(i32);
impl From<i32> for ChildExitStatus {
    fn from(exit_status: i32) -> Self {
        ChildExitStatus(exit_status)
    }
}
#[derive(Debug, thiserror::Error)]
#[error("{0}")]
pub(crate) struct LogicError(String);
impl LogicError {
    pub fn new(message: impl ToString) -> Self {
        LogicError(message.to_string())
    }
}
#[derive(Debug, thiserror::Error)]
#[error("child process io failed: {0:?}")]
pub(crate) struct ChildIoError(#[from] pub io::Error);

pub fn assert_child_io<T>(cmd: impl fmt::Debug, result: io::Result<T>) -> PassFuResult<T> {
    result
        .map_err(ChildIoError)
        .ctx(&format!("{cmd:?}"))
        .ctx("running child")
}

pub fn assert_child_status(
    cmd: impl fmt::Debug,
    status: io::Result<std::process::ExitStatus>,
) -> PassFuResult<()> {
    let code = status
        .map_err(ChildIoError)
        .ctx(&format!("{cmd:?}"))
        .ctx("running child")?;

    if code.success() {
        Ok(())
    } else {
        Err(ChildExitStatus(code.code().unwrap_or(255)))
            .ctx(&format!("{cmd:?}"))
            .ctx("running child")
    }
}
