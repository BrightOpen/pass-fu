# Pass-Fu

<a href="https://crates.io/crates/pass-fu"><img src="https://img.shields.io/crates/v/pass-fu.svg" alt="pass fu crates version"></a>

Implemented in vanila pass:

- `pass show`
  print the whole file on the output
- `pass show --qrcode[=N]`
  shows a neat QR code for the selected or first line
- `pass show --clip[=N]`
  copy the chosen line number or first line to clipboard

The `pass` manager is solid. UI apps are cute. And for efficiency - Pass-Fu.

Pass-Fu is extending the pass manager:

* [x] `show --output=N` 
      print only the chosen line to the output (zero based)
* [x] **entry picker** - if the path is not rooted or is a directory
      you'll be prompted to chose one (`dmenu`, `wofi`...)
* [x] **line picker** - if the file has multiple lines
      you'll be prompted to chose one (`dmenu`, `wofi`...)
* [x] **OTP handler** - automatically recognize OTP URI and let you choose 
      if the OTP secret or OTP code is desired (`dmenu`, `wofi`...)
* [x] `show --type=N`
      directly type the input (`xdotool`, `ydotool`,...) 
* [ ] make it more configurable, currently hardcoded options
* [ ] show new opts in help

Pass-Fu is aiming to overlay the pass manager - let it do what it does well 
and improve it where it's lacking. You can `alias pass=pass-fu` as pass-fu 
tries to be compatible. pass-fu still calls the original and the alias 
will preserve autocompletion.

# Install

Rustaceans:

```
cargo install pass-fu
```

Then make it your default pass interface (in .profile):

```
alias pass=pass-fu
```

Or with a symlink to your local bin folder:

```
ln -s "$(which pass-fu)" ~/bin/pass
```

# Usage

Currently, only the `pass show` command is extended:

Use pass as normally. If you show a dir or omit the path, dmenu prompt will appear with a list of entries. Then if the file has multiple lines, it will let you chose the line. And if the line is an OTP secret URL, it will also let you generate the token.

Besides the `--clip` (`-c`) and `--qrcode` (`-q`) options, there is:
* `--type` (`-t`) to type the secret
* `--output` (`-o`) to write to output

All these take a line number arg, for instance `--type=2` but that's not too useful since you have that picker anyway.

